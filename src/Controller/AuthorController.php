<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;

/**
 * @Route("sharebook-api/authors"), name="authors"
 */
class AuthorController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/add", methods="POST")
     */
    public function addAuthor(Request $request, EntityManagerInterface $managerInterface)
    {
        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author);
        $form->submit(json_decode($request->getContent(), true));        

        if ($form->isSubmitted() && $form->isValid()) {
            $managerInterface->persist($author);
            $managerInterface->flush();

            return new JsonResponse($this->serializer->serialize($author, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route(methods="GET")
     */
    public function readAllAuthors(AuthorRepository $authorRepo)
    {
        $authors = $authorRepo->findAll();
        $json = $this->serializer->serialize($authors, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{author}", methods="GET")
     */
    public function readOneBook(Author $author)
    {
        return new JsonResponse($this->serializer->serialize($author, 'json'), 200, [], true);
    }

}
