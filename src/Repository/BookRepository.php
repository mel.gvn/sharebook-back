<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findByTitleAsc($word)
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.title LIKE :word')
            ->setParameter(':word', '%' . $word . '%')
            ->orderBy('book.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByTitleDesc($word)
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.title LIKE :word')
            ->setParameter(':word', '%' . $word . '%')
            ->orderBy('book.title', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByFour($word)
    {
        return $this->createQueryBuilder('book')
            ->andWhere('book.title LIKE :word')
            ->setParameter(':word', '%' . $word . '%')
            ->orderBy('book.title', 'DESC')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }

    // public function findByCategory($word)
    // {
    //     return $this->createQueryBuilder('book')
    //     ->andWhere('book.categories LIKE :word')
    //     ->setParameter(':word', '%' . $word . '%')
    //     // ->orderBy('book.title', 'DESC')
    //     ->getQuery()
    //     ->getResult();
    // }
    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
