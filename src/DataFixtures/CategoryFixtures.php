<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryFixtures extends Fixture
{

    public const ROMAN_HISTORIQUE_REFERENCE = 'romanHistorique';
    public const RECUEIL_DE_NOUVELLES_REFERENCE = 'recueilDeNouvelles';
    public const DRAME_REFERENCE = 'drame';
    public const DRAME_ROMANTIQUE_REFERENCE = 'drameRomantique';
    public const ROMAN_REFERENCE = 'roman';

    public function load(ObjectManager $manager)
    {
        // Categories
        $romanHistorique = new Category();
        $romanHistorique->setTitle('Roman historique');

        $recueilDeNouvelles = new Category();
        $recueilDeNouvelles->setTitle('Recueil de nouvelles');

        $drame = new Category();
        $drame->setTitle('Drame');

        $drameRomantique = new Category();
        $drameRomantique->setTitle('Drame romantique');

        $roman = new Category();
        $roman->setTitle('Roman');

        $manager->persist($romanHistorique);
        $manager->persist($recueilDeNouvelles);
        $manager->persist($drame);
        $manager->persist($drameRomantique);
        $manager->persist($roman);

        $manager->flush();

        $this->addReference(self::ROMAN_HISTORIQUE_REFERENCE, $romanHistorique);
        $this->addReference(self::RECUEIL_DE_NOUVELLES_REFERENCE, $recueilDeNouvelles);
        $this->addReference(self::DRAME_REFERENCE, $drame);
        $this->addReference(self::DRAME_ROMANTIQUE_REFERENCE, $drameRomantique);
        $this->addReference(self::ROMAN_REFERENCE, $roman);
    }
}
